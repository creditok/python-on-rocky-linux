# syntax=docker/dockerfile:1

FROM python:3.12-bookworm

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt update && apt install -y \
    git \
    gcc \
    && rm -rf /var/lib/apt/lists/* \
    && pip install --upgrade --no-cache-dir setuptools wheel \
    && pip install --upgrade --no-cache-dir pdm
